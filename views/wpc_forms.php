<?php

$forms = Wpc_get_forms();

?>
<div class="wrap">
    
    <div id="icon-tools" class="icon32"><br /></div>    
    	
    <h2> Forms</h2>
    
    
    <table cellpadding="2" cellspacing="1" class="widefat" width="50%">
         
         <thead>
             <tr>
                 <th scope="col">Title</th>
                 <th scope="col">Shortcode</th>
                 <th scope="col">Date</th>
             <tr>
         </thead>
         
         <tbody>
             
             <?php if(count($forms)>0){
                 
                 $b=0;
                 
                 foreach($forms as $frm){ 
                     
                     if($b%2==0)
                        $class=' class="alternate"';
                    else
                        $class='';                
                     
                     ?>
                     
                    
             <tr<?php echo $class;?>>
                 <td><?php echo $frm->var_name;?></td>
                 <td></td>
                 
             </tr>
                     
                 <?php }                 
                 
             }else{?>
                 
             <tr class="alternative">
                 <td colspan="2">No forms found</td>
             </tr>
             
                 
             <?php } ?>
             
         </tbody>
         
    </table>         
    
</div>

