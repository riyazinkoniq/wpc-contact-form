<?php
/* To add a new form */

?>
<div class="wrap">
    
    <div id="icon-tools" class="icon32"><br /></div>    
    	
    <h2> Add Form</h2>
    
    <div class="container">
      <div class="row clearfix">
        <!-- Building Form. -->
        <div class="span8">
          <div class="clearfix">            
            
            <div id="build">
              <form id="target" class="form-horizontal">
                  
                  
              </form>
            </div>
          </div>
        </div>
        <!-- / Building Form. -->

        <!-- Components -->
        <div class="span6">
          <h2>Drag & Drop components</h2>
          <hr>
          <div class="tabbable">
            <ul class="nav nav-tabs" id="formtabs">
              <!-- Tab nav -->
            </ul>
            <form class="form-horizontal" id="components">
              <fieldset>
                <div class="tab-content">
                  <!-- Tabs of snippets go here -->
                </div>
              </fieldset>
            </form>
          </div>
        </div>
        <!-- / Components -->

      </div>

      
    </div> <!-- /container -->

    
</div>

