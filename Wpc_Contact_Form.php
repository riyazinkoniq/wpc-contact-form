<?php

class Wpc_Contact_Form{
    
    protected function __construct() {
				
         	
    }
	
	/**
     * Get the absolute system path to the plugin directory, or a file therein
     * @static
     * @param string $path
     * @return string
     */
    protected static function plugin_path( $path ) {
        $base = dirname(__FILE__);
        if ( $path ) {
            return trailingslashit($base).$path;
        } else {
            return untrailingslashit($base);
        }
    }
	
	/**
     * Get the plugin url for a file therein
     * @static
     * @param string $path
     * @return string
     */
    protected static function plugin_url( $url ) {
        $base = site_url();
        if ( $url ) {
            return $base."/wp-content/plugins/".AJABFOLDER."/".$url;
        } else {
            return $url;
        }
    }
}