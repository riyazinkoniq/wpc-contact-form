/* 
 * Scripts for Stunnerweb
 * 
 */
jQuery(window).load(function(){
    
    jQuery("#stn-builder-button").click(function(){
        
        var activelabel = jQuery(this).attr('data-active-button');
        
        var inactivelabel = jQuery(this).attr('data-inactive-button');
        
        jQuery(this).text(inactivelabel);
        
        jQuery(this).attr('data-active-button',inactivelabel);
        
        jQuery(this).attr('data-inactive-button',activelabel);
        
        jQuery("#postdivrich").toggle();
        
        jQuery("#stn_visual_editor_meta_box").toggle();
        
              
    });
    
    jQuery(".stn_layouts").click(function(){
        
        var classval = jQuery(this).attr('id'); 
                        
        var elt = '<div class="stn_layout_column '+classval+'"><div class="stn_meta_header"><div class="stn_remove">X</div><div class="stn_content"></div></div></div>';
        
        jQuery("#stn_layout_builder").append(elt);
        
        stn_remove_elements();
    });
    
    $(function() {
        $( "#sortable" ).sortable({
            start: function (event, ui) {
              
            },
            change:  function (event, ui) {
               
            },
            stop:function (event, ui) {
             
             var stn_txt='';                
                
             $(".stn_hidden_txt").each(function(){
            
                stn_txt+= $(this).text();
            });
             $("#stunner_content").text(stn_txt);
            }
        });
        
        $( "#sortable" ).disableSelection();
  });
    
       
});

function stn_remove_elements(){
    
     jQuery(".stn_remove").click(function(){ 
        
        jQuery(this).closest(".stn_layout_column").remove();
    });
}