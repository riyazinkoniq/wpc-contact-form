<?php
/*
Plugin Name: Wordpress Contact Form
Version: 1.0
Description: This plugin was used to create contact forms.
Author: INKONIQ
Contributors: Mohamed Riyaz
Author URI: http://www.inkoniq.com
*/

if(!function_exists('Wpc_class_autoloader')){
	
	function Wpc_class_autoloader($class){
		$this_dir = dirname(__FILE__);
		$dirs = array(
                    $this_dir,
                    $this_dir.DIRECTORY_SEPARATOR.'config',
                    $this_dir.DIRECTORY_SEPARATOR.'admin'                    
                    );
		foreach ( $dirs as $dir ) {
        
			if ( file_exists($dir.DIRECTORY_SEPARATOR.$class.'.php') ) {
				
				include_once($dir.DIRECTORY_SEPARATOR.$class.'.php');
                    
				break;
			}
		}
		
		if(file_exists(dirname(__FILE__).DIRECTORY_SEPARATOR.$class.'.php')){
			
			require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.$class.'.php');
		
		}		
		
	}
        
	spl_autoload_register('Wpc_class_autoloader');
        
	register_activation_hook( __FILE__, 'Wpc_install_plugin' ); 
        
	Wpc_Config::init();
        
	Wpc_Plugin_Actions::init();
                
	function Wpc_install_plugin(){            
				
		global $wpdb;

		$F_sql = "CREATE TABLE `".$wpdb->prefix."wpc_forms` (`int_ID` int(11) NOT NULL AUTO_INCREMENT,`var_name` varchar(255) NOT NULL,       
             `txt_content` text NOT NULL,`dt_cdate` datetime NOT NULL,`dt_mdate` datetime NOT NULL,PRIMARY KEY (`int_ID`))";
		
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		
		dbDelta( $F_sql );	
                      
	}
	
}