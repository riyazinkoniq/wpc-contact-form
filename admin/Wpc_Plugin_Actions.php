<?php

class Wpc_Plugin_Actions extends Wpc_Contact_Form{
    
    protected function __construct() {			
        
        add_action('admin_print_scripts', array(&$this, 'Wpc_load_scripts') );		
		
        add_action('admin_print_styles', array(&$this, 'Wpc_load_styles') );   
                
        add_action('admin_menu',array(&$this,'Wpc_add_menu'));
		
	add_action('init',array(&$this,'Wpc_load_lib'));
        
    }
	
    public function Wpc_load_lib(){
	
        require_once(self::plugin_path('/lib/Wpc_functions.php'));
		
    }
	
    public function Wpc_add_menu(){
		
        add_menu_page('Contact Form','Contact Form','manage_options',WPC_FOLDER,array($this,'Wpc_menu_action'));  
		
	add_submenu_page(WPC_FOLDER,'Forms','Forms','manage_options',WPC_FOLDER,array($this,'Wpc_menu_action'));  
		
	add_submenu_page(WPC_FOLDER,'Add Form','Add Form','manage_options','wpc_add_form',array($this,'Wpc_menu_action'));  
		
	add_submenu_page(WPC_FOLDER,'Contact Form','Options','manage_options','wpp_form_options',array($this,'Wpc_menu_action'));  		
				
    }       
	
    public function Wpc_menu_action(){
		
        $page = $_GET['page'];
		
	switch ($page){
			
            case WPC_FOLDER:
				
		require_once(self::plugin_path('/views/wpc_forms.php'));
				
            break;
			
            case 'wpc_add_form':
			
                require_once(self::plugin_path('/views/wpc_addform.php'));
			
            break;
			
            case 'wpp_form_options':
			
            break;
			
            default:
			
            break;			
		
	}
	
    }
        
    function Wpc_load_scripts(){
        
        echo '<script data-main="'.WP_PLUGIN_URL.'/'.WPC_FOLDER.'/resources/js/main-built.js" src="'.WP_PLUGIN_URL.'/'.WPC_FOLDER.'/resources/js/lib/require.js"></script>';
        
    }
    
    function Wpc_load_styles(){        
        
        $page = $_GET['page'];
        
        echo "<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>";
        
        if($page=='wpc_add_form'){            
            
            wp_enqueue_style( 'bootstrapmin', WP_PLUGIN_URL .'/'.WPC_FOLDER.'/resources/css/lib/bootstrap.min.css', array() );
            
            wp_enqueue_style( 'bootstrapmin', WP_PLUGIN_URL .'/'.WPC_FOLDER.'/resources/css/lib/bootstrap-responsive.min.css', array() );
            
            wp_enqueue_style( 'bootstrapcustom', WP_PLUGIN_URL .'/'.WPC_FOLDER.'/resources/css/custom.css', array() );
            
        }
        
        wp_enqueue_style( 'wpc_styles', WP_PLUGIN_URL .'/'.WPC_FOLDER.'/resources/css/wpc_styles.css', array() );
    }    
    
    /* Start Singleton */
    private static $instance;
    
    public static function init() {
    
        self::$instance = self::get_instance();
    
    }
    
    public static function get_instance() {
        	
        if ( !is_a(self::$instance, __CLASS__) ) {
            	
            self::$instance = new self();
        
        }
        
        return self::$instance;
    }
    
    final public function __clone() {
        	
        trigger_error("No cloning allowed!", E_USER_ERROR);
    
    }    
    
    final public function __sleep() {
        	
        trigger_error("No serialization allowed!", E_USER_ERROR);
		
    }
    /* End Singleton */
}