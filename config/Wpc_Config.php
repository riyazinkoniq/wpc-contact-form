<?php

class Wpc_Config{
    
    protected function __construct() {
        
        self::Wpc_define_constants();				
         	
    }
    
    public function Wpc_define_constants(){
        
        // required for Windows & XAMPP
		define('WINABSPATH', str_replace("\\", "/", ABSPATH) );
			
		// define URL
		define('WPC_FOLDER', basename( dirname(dirname(__FILE__) ) ) );
		
		define('WPC_ABSPATH', trailingslashit( str_replace("\\","/", WP_PLUGIN_DIR . '/' . WPC_FOLDER ) ) );
			
		define('WPC_URLPATH', trailingslashit( plugins_url( WPC_FOLDER ) ) );	
        
    }   
    

    /* Start Singleton */
    private static $instance;
    
    public static function init() {
    
        self::$instance = self::get_instance();
    
    }
    
    public static function get_instance() {
        	
        if ( !is_a(self::$instance, __CLASS__) ) {
            	
            self::$instance = new self();
        
        }
        
        return self::$instance;
    }
    
    final public function __clone() {
        	
        trigger_error("No cloning allowed!", E_USER_ERROR);
    
    }
    
    
    final public function __sleep() {
        	
        trigger_error("No serialization allowed!", E_USER_ERROR);
		
    }
    /* End Singleton */
}